# -*- coding: utf-8 -*-
# Классификация сайтов на основе тегов adroom

import sys
import pandas as pd
import numpy as np
import ipdb

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.cross_validation import train_test_split
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB, BernoulliNB, GaussianNB
from sklearn import metrics
import scipy.sparse as sp

# def group_by_domain(data, feature_field = 'text', target_field = 'tag_name', group_by = "domain_id"):
#     domains = dict()
#     words_by_domain = dict()
#     print "read file..."
#     for idx, row in data.iterrows():
#         domain = row[group_by]
#         text = row[feature_field]
#         target = row[target_field]
#         domains[domain] = target
#         word_map = words_by_domain.get(domain)
#         if not word_map:
#             word_map = dict()
#             words_by_domain[domain] = word_map
#         for w in text.split(" "):
#             if (len(w) > 1):
#                 word_map[w] = (word_map.get(w) or 0) + 1
#
#     print "calc uniq words..."
#     uniq_words = set()
#     for _, word_map in words_by_domain.items():
#         for w in word_map.keys():
#             uniq_words.add(w)
#     uniq_words = [w for w in uniq_words]
#     x = sp.lil_matrix((len(domains), len(uniq_words)), dtype=int)
#     y = []
#     d_idx = 0
#     print "create X..."
#     uniq_words_idxs = dict((v, i) for i, v in enumerate(uniq_words))
#     for domain, word_map in words_by_domain.items():
#         tag = domains[domain]
#         y.append(tag)
#         for word, count in word_map.items():
#             w_idx = uniq_words_idxs[word]
#             x[d_idx, w_idx] = count
#         d_idx += 1
#
#     print "ready to ML..."
#     return x, np.array(y)

csv_filename = (len(sys.argv) > 1 and sys.argv[1]) or 'url_tags.csv'
data = pd.read_csv(csv_filename, header=None, sep="\t")
data.columns = ['id', 'text', 'tag_name']
def deserialize_text(text):
    pairs = text.split(",")
    fill_text = ''
    for w, c in map(lambda x: x.split("="),pairs):
        fill_text += (w + ' ') * int(c)
    return fill_text
data['text'] = map(deserialize_text, data['text'])

def simple_tt_split(x, y, test_size=0.2):
    idx = int(len(x) * (1 - test_size))
    return (x[:idx], x[idx:], y[:idx], y[idx:])


#data = data[~pd.isnull(data['title'])]
#data['text'] = (data['title'] + data['description'].apply(lambda x: '' if pd.isnull(x) else ' ' + x))
X = data['text']
y = data['tag_name']
X_train, X_test, y_train, y_test = simple_tt_split(X, y, test_size=0.2)

count_vect = CountVectorizer()
tfidf_transformer = TfidfTransformer(use_idf=False)

def pipeline(texts):
    return count_vect.transform(texts)
    #return tfidf_transformer.transform(count_vect.transform(texts))

def accuracy(pred, ys):
    a = sum(pred == ys) / float(len(ys))
    #print "Accuracy: %f" % a
    return a


X_train_counts = count_vect.fit_transform(X_train)
X_train_tf = X_train_counts # tfidf_transformer.fit_transform(X_train_counts)
print "Data loaded and parsed"

for alpha in np.linspace(1.0, 2.0, 5):
    clf = MultinomialNB(alpha=alpha)
    clf.fit(X_train_tf, y_train)
    pred = clf.predict(pipeline(X_test))
    clf.predict_log_proba(pipeline(X_test))
    acc = accuracy(pred, y_test)
    print "Alpha=%.4f, accuracy=%.4f" % (alpha, acc)
    #import ipdb; ipdb.set_trace()


sys.exit(0)

m = metrics.classification_report(y_test, pred)
m = [filter(lambda x:x, l.split("      ")) for l in m.split("\n")]
for l in m:
    for f in l:
        print f[:10],
        print "\t",
    print "\n",

import ipdb; ipdb.set_trace()

