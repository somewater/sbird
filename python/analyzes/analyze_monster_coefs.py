import os
import sys
import numpy as np
import pandas as pd
from sklearn import linear_model
from sklearn.cross_validation import cross_val_score
import scipy.optimize
from math import *

if len(sys.argv) != 3:
    exit("Call as: program <data csv> <ctrs csv>")

# функции для составления новых фич
def log2(x): return log(0.00000001 + x, 2)
def logN(x): return log(0.00000001 + x)
def pow0_5(x): return pow(x, 0.5)
def pow1_1(x): return pow(x, 1.1)
def pow2_0(x): return pow(x, 2.0)

# оценки алгоритмов и ctr пишутся в разные файлы, мерджим их и оставляем только пары (страница -> рекомендация для неё),
# для которых известны и оценки и реальный ctr
weights = pd.read_csv(sys.argv[1], sep="\t", index_col=0)
ctr = pd.read_csv(sys.argv[2], header=None, sep="\t", index_col=0, names=['ctr'])
data = pd.merge(weights, ctr, left_index=True, right_index=True, how='inner')
data = data[data.ctr > 0]

X = data[data.columns[0:-1]]
# в базе и на выходе алгоритмов у нас вес в смысле меньше-лучше, поэтому делим на него,
# чтобы получить весь в человеческом смысле больше->лучше
X = X.applymap(lambda x: 1.0/x if x > 0 else 0)
y = data[data.columns[-1]]

import ipdb; ipdb.set_trace()

original_cols = X.columns.tolist()

# новые фичи: произведение признаков

# for col in original_cols:
#     for col2 in original_cols[(original_cols.index(col) + 1):]:
#         if col != col2:
#             new_col = col + "_" + col2
#             X[new_col] = X[col] * X[col2]

# новые фичи: функции от признаков

for func, func_name in [(logN, 'logN')]: # согласно тестам, только log увеличивал качество
    for col in original_cols:
        new_col = col + "_" + func_name
        X[new_col] = X[col].apply(func)

print X.columns

# регрессор с возможностью задавать свою ф-ю регуляризации
# (нужно, чтобы склонять его к выдаче неотрицательных коэффициентов)
class PositiveWeightsEstimator():

    # reg_cost_func - ф-я расчета штрафа регуляризатора и градиента
    # const_feature - добавить ли в матрицу признаков константный признак
    def __init__(self, reg_cost_func, C = 1.0, const_feature = True, minimize_method = 'Nelder-Mead'):
        self.reg_cost_func = reg_cost_func
        self.C = C
        self.coef_ = None
        self.const_feature = const_feature
        self.minimize_method = minimize_method

    def fit(self, X, y):
        C = self.C
        if self.const_feature:
            X = np.hstack((np.ones((X.shape[0], 1), dtype=float), X))
        initial_coef_ = np.zeros(X.shape[1])

        def objective(W):
            # compute training cost/grad
            cost, outer_grad = self.l2_cost_func(np.dot(X, W) - y)
            grad = np.dot(outer_grad, X)  # chain rule

            # add regularization cost/grad (but don't regularize intercept)
            if self.const_feature:
                reg_cost, reg_grad = reg_cost_func(W[1:])
                grad[1:] += C * reg_grad
            else:
                reg_cost, reg_grad = self.reg_cost_func(W)
                grad += C * reg_grad
            cost += C * reg_cost

            return cost, grad

        res = scipy.optimize.minimize(
            objective, initial_coef_, jac=True, method=self.minimize_method)

        if res.success:
            self.coef_ = map(lambda x: x if x > 0 else 0, res.x)
        else:
            raise Exception("Fit failed: {}".format(res.message))

    def predict(self, X):
        if self.const_feature:
            return np.dot(X, self.coef_[1:]) + self.coef_[0]
        else:
            return np.dot(X, self.coef_)

    def l2_cost_func(self, z):
        N = len(z)
        return 0.5 * np.dot(z, z) / N, z / N

NegCoef = 1.0 # какой "штраф" получают отрицательные веса в регуляризаторе
def reg_cost_func(z):
    N = len(z)
    #return 0.5 * np.dot(z, z) / N, z / N # L2
    # return np.sum(np.abs(z)) / N, np.sign(z) / N # L1
    # print "z=%s" % repr(z)
    z2 = np.array(map(lambda x: x if x > 0 else x * NegCoef, z))
    #return 0.5 * np.dot(z2, z2) / N, z2 / N
    return np.sum(np.abs(z2)) / N, np.sign(z2) / N

methods = ['Nelder-Mead',
           'Powell',
           'CG',
           'BFGS',
           #'Newton-CG', # не сходится
           'L-BFGS-B',
           'TNC',
           'COBYLA',
           'SLSQP',
           'dogleg',
           'trust-ncg']

# запускаем все варианты и считаем качество (сумму модулей ошибок). Пока что качество ужасное
modes = []
const_feature = True
for cf in [True, False]:
    const_feature = cf
    for neg_coef in [1, 5, 10, 100, 500, 1000, 5000, 10000, 500000, 1000000]:
        NegCoef = neg_coef
        for c in np.linspace(0.0000001, 1.0, 10):
            for method in methods:
                mode = "const_feature = %s, NegCoef = %d, C = %.2f, METHOD %s" % (str(const_feature), NegCoef, c, method)
                print mode
                #estimator = linear_model.LinearRegression()
                estimator = PositiveWeightsEstimator(reg_cost_func=reg_cost_func,
                                                     const_feature=const_feature,
                                                     minimize_method=method,
                                                     C=c)
                try:
                    estimator.fit(X, y)
                except Exception:
                    print "  Error"
                    continue

                #print cross_val_score(estimator, X.values, y.values, scoring='r2')
                sum_error = sum(map(lambda x: abs(x), estimator.predict(X) - y))
                modes.append((sum_error, mode, estimator.coef_))
                print "  error: %.2f, sum(abs): %.2f" % (sum_error, sum(abs(y)))
                print "  coef_: %s" % "\n         ".join(map(lambda x: "%s: %.10f" % x, sorted(zip(X.columns, estimator.coef_[(1 if const_feature else 0):]), key=lambda x: x[1])))

print "============"
print "\n".join(map(str, sorted(modes, key=lambda x: x[0])))
#import ipdb; ipdb.set_trace()