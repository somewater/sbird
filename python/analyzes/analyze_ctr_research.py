import os
import sys
import numpy as np
from collections import namedtuple
import matplotlib.pyplot as plt
from matplotlib.dates import date2num, datestr2num
import scipy as sp
from scipy import signal

sys.path.append(os.getcwd() + '/..')
from utils.database import *

def select(db_conn, sql):
    cursor = db_conn.cursor()
    cursor.execute(sql)
    return cursor.fetchall()

def ctr(c):
    return float(c.clicks) / c.views

def all_avg(cc):
    return float(np.sum(map(lambda c: c.clicks, cc))) / np.sum(map(lambda c: c.views, cc))

Ctr = namedtuple('Ctr', ['url_id', 'domain_id', 'views', 'clicks', 'ctime'])
ctr_research = [Ctr(*v) for v in select(recs, "SELECT * FROM ctr_research")]
domains = select(b2b, "SELECT id, domain FROM domains WHERE id IN (%s)" %
                 ",".join(np.unique(map(lambda x: str(x.domain_id), ctr_research))))
domains = dict((id, name) for (id, name) in domains)


print "Start plotting"
for (domain_id, domain_name) in domains.items():
    print "  Domain %s" % domain_name
    domain_urls = sorted(filter(lambda x: x.domain_id == domain_id, ctr_research), key=lambda c: c.ctime)


    ctr_avg_by_hour = dict()
    for c in domain_urls:
        ctr_avg_by_hour.setdefault(c.ctime.hour, []).append(ctr(c))
    for hour in ctr_avg_by_hour.keys():
        ctr_avg_by_hour[hour] = np.mean(ctr_avg_by_hour[hour])

    ctr_avg = dict()
    for url_id in np.unique(map(lambda x: x.url_id, domain_urls)):
        urls = filter(lambda x: x.url_id == url_id, domain_urls)
        times = map(lambda c: date2num(c.ctime), urls)
        ctrs = map(lambda c: ctr(c), urls)

        # plt.plot_date(times, ctrs, '-', alpha=0.3)

        for idx, c in enumerate(urls):
            time = str(c.ctime)
            if not ctr_avg.get(time):
                ctr_avg[time] = []
            ctr_avg[time].append(c)

    ctr_avg = sorted(ctr_avg.items(), key=lambda x: x[0])
    ctr_avg_keys = [datestr2num(time) for (time, _) in ctr_avg]
    ctr_avg_values = [all_avg(cc) for (_, cc) in ctr_avg]

    ctr_avg_values_blur = sp.signal.medfilt(ctr_avg_values, 21)
    z = np.polyfit(ctr_avg_keys, ctr_avg_values, 1)
    p = np.poly1d(z)
    xp = np.linspace(min(ctr_avg_keys), max(ctr_avg_keys), 100)
    print "%s, z = %s" % (domain_name, repr(z))

    plt.plot_date(ctr_avg_keys, ctr_avg_values, '-', linewidth=1, color='y')
    plt.plot_date(xp, p(xp), '-', linewidth=2, color='b')
    plt.plot_date(ctr_avg_keys, ctr_avg_values_blur, '-', linewidth=1, color='r')

    plt.show()
    #raw_input("Next domain...")
    plt.clf()
import ipdb; ipdb.set_trace()
