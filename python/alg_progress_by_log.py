# -*- encoding: utf-8 -*-
import re
import os
import pyperclip
import matplotlib.pyplot as plt
from collections import namedtuple
from matplotlib.patches import Rectangle

# график точек окончания обработки доменов
def parse_domain_ends(s):
    R = re.compile('(\d+):(\d+) DomainId  .+: end')
    xs = {}
    for l in s.split("\n"):
        for h, m in R.findall(l):
            min = int(h) * 60 + int(m)
            xs[min] = (xs.get(min) or 0) + 1
    plt.clf()
    plt.scatter(xs.keys(), [v for k,v in xs.items()])

def parse_duration(s, start_re, end_re):
    host_start = {}
    host_duration = {}
    for l in s.split("\n"):
        for h, m, host in start_re.findall(l):
            sec = int(h) * 60 + int(m)
            host_start[host] = sec
        for h, m, host in end_re.findall(l):
            sec = int(h) * 60 + int(m)
            start = host_start.get(host)
            if start:
                host_duration[host] = sec - start
    return host_duration

# длительности расчета рекомендаций
def parse_host_calc(s):
    start = re.compile('(\d+):(\d+) Domain start  ([^\s]+): \d+')
    end = re.compile('(\d+):(\d+) DomainId  ([^\s]+): end \d+')
    v = parse_duration(s, start, end)

    start_id = re.compile('(\d+):(\d+) Domain start  ([^\s]+): (\d+)')
    v2 = {}
    for l in s.split("\n"):
        for h, m, host_name, host_id in start_id.findall(l):
            v2[int(host_id)] = v[host_name]
    return v2


# длительности записи рекомендаций
def parse_host_write(s):
    start = re.compile('(\d+):(\d+) Writer  (\d+): \d+')
    end = re.compile('(\d+):(\d+) Writer  end: (\d+)')
    return parse_duration(s, start, end)

def plot_hash(xs):
    plt.clf()
    plt.scatter(xs.keys(), [v for k,v in xs.items()])

def process(fn, line):
    buffer = []
    if (len(fn) > 1000):
        s = fn
        for l in s.split("\n"):
            v = line(l)
            if v and len(v) > 0:
                buffer.append(v)
    else:
        with open(fn) as f:
            for l in f:
                v = line(l)
                if v and len(v) > 0:
                    buffer.append(v)
    return buffer

# подсчитать общее время, затраченное на разные типы работы
def bench_stat(fn):
    r = re.compile('BENCHMARK: ([^\,]+),.+time=(\d+) ms')
    h = dict()
    def line(l):
        for name, ms in r.findall(l):
            h[name] = (h.get(name) or 0) + int(ms)
    process(fn, line)
    return h

# сумма GC
def gc_sum(fn):
    GC = re.compile("(\[GC|\[Full GC) .+, ([\d\,\.]+) secs\]") # [time, gc_sec_float]
    def line(l):
        for gc_name, gc_secs in GC.findall(l):
            return [float(gc_secs.replace(',', '.'))]
    return sum(map(lambda x: x[0], process(fn, line)))

# построить график, с отметками используемой памяти и моментов расчета-записи
def history_graph(fn, fromAzkaban=True, useWriteBench=True):
    EventType = namedtuple('EventType', ['color', 'alpha', 'zorder'])
    gc = EventType(color='red', alpha=1.0, zorder=1)
    calc = EventType(color='green', alpha=0.2, zorder=0)
    write = EventType(color='blue', alpha=0.2, zorder=0)

    Event = namedtuple('Event', ['time', 'duration', 'type', 'label']) # time - sec, duration - sec (float)
    Point = namedtuple('Event', ['time', 'value'])

    global START_TIME
    START_TIME = 0
    events = []
    points = []

    if fromAzkaban:
        line_start = '^[\d\-]+ (\d\d:\d\d:\d\d) .+ '
    else:
        line_start = '^(\d\d:\d\d) '
    TIME = GC = re.compile(line_start)
    GC = re.compile("%s(\[GC|\[Full GC) .+, ([\d\.]+) secs\]" % line_start) # [time, gc_name, gc_sec_float]
    CALC_START = re.compile("%sDomain start  ([^\s]+): (\d+)" % line_start) # [time, host_name, host_id]
    CALC_END = re.compile("%sDomainId  ([^\s]+): end \d+" % line_start) # [time, host_name]

    WRITE_START = re.compile("%sWriter  (\d+): \d+" % line_start) # [time, host_id]
    WRITE_END = re.compile("%sWriter  end: (\d+)" % line_start) # [time, host_id]
    WRITE_BENCH = re.compile("%sBENCHMARK: write success, recs=\d+ domainIds=([^\s]+) time=(\d+) ms, memory=\d+ Mb" % line_start) # [time, host_name, ms]

    MEM_USAGE = re.compile("%s.+memory=(\d+) Mb" % line_start) # [time mem_mb]

    domain_calc_start = dict()
    domain_write_start = dict()
    domain_name_by_id = dict()

    def parse_time_to_sec(time):
        try:
            if fromAzkaban:
                h, m, s = map(int, time.split(":"))
            else:
                h = 0
                m, s = map(int, time.split(":"))
        except Exception:
            print "Unparsed time:", time
        return h * 60 * 60 + m * 60 + s

    def process_line(l):
        global START_TIME
        if START_TIME == 0:
            for time in TIME.findall(l):
                START_TIME = parse_time_to_sec(time)

        for time, mem_mb in MEM_USAGE.findall(l):
            point = Point(parse_time_to_sec(time), int(mem_mb))
            points.append(point)

        for time, gc_name, gc_sec_float in GC.findall(l):
            event = Event(parse_time_to_sec(time), float(gc_sec_float), type=gc, label=None)
            events.append(event)
            return

        for time, host_name, host_id in CALC_START.findall(l):
            domain_calc_start[host_name] = parse_time_to_sec(time)
            domain_name_by_id[host_id] = host_name
            return

        for time, host_name in CALC_END.findall(l):
            calc_start = domain_calc_start.get(host_name)
            if calc_start:
                calc_end = parse_time_to_sec(time)
                if calc_end == calc_start:
                    calc_end += 0.001
                label = None
                if calc_end - calc_start > 60:
                    label = host_name
                event = Event(calc_start, calc_end - calc_start, type=calc, label=label)
                events.append(event)
            return

        if useWriteBench:
            for time, host_name, ms in WRITE_BENCH.findall(l):
                duration_sec = int(ms) * 0.001
                write_start = parse_time_to_sec(time) - duration_sec
                label = None
                if duration_sec > 60:
                    label = host_name
                event = Event(write_start, duration_sec, type=write, label=label)
                events.append(event)
        else:
            for time, host_id in WRITE_START.findall(l):
                domain_write_start[host_id] = parse_time_to_sec(time)
                return

            for time, host_id in WRITE_END.findall(l):
                write_start = domain_write_start.get(host_id)
                if write_start:
                    host_name = domain_name_by_id.get(host_id) or 'undefined'
                    write_end = parse_time_to_sec(time)
                    if write_end == write_start:
                        write_end += 0.001
                    label = None
                    if write_end - write_start > 60:
                        label = host_name
                    event = Event(write_start, write_end - write_start, type=write, label=label)
                    events.append(event)
                return


    process(fn, process_line)

    max_time = 0
    max_duration = 0
    max_value = max(map(lambda p: p.value, points)) if len(points) > 0 else None

    fig, cax = plt.subplots()
    #cax = plt.gca()
    cax2 = cax.twinx()
    print "Events count=", len(events)
    for ev in events:
        cax.add_patch(Rectangle((ev.time - START_TIME, 0), ev.duration, ev.duration, alpha=ev.type.alpha,
                                facecolor=ev.type.color, zorder=ev.type.zorder, edgecolor='none'))
        if ev.label and len(ev.label) > 0:
            cax.text(ev.time - START_TIME, ev.duration * 1.02, ev.label, fontsize=10, zorder=2)
        if ev.time > max_time:
            max_time = ev.time
        if ev.duration > max_duration:
            max_duration = ev.duration

    cax2.scatter(map(lambda p: p.time - START_TIME, points), map(lambda p: p.value, points), zorder=1,
                 facecolors='darkgoldenrod', edgecolors='none', marker='.')

    cax.set_xlabel('sec')
    cax.set_ylabel('sec')
    cax2.set_ylabel('Mb', color='darkgoldenrod')
    for tl in cax2.get_yticklabels():
        tl.set_color('darkgoldenrod')

    cax.set_xlim((0, (max_time - START_TIME) * 1.05))
    cax.set_ylim((0, max_duration * 1.05))
    cax2.set_xlim((0, (max_time - START_TIME) * 1.05))
    if max_value:
        cax2.set_ylim((0, max_value * 1.05))
    plt.show()


def hash_to_persent(hash):
    all = sum(hash.values())
    return dict((k, "%.2f %%" % (float(v) / all * 100)) for k,v in hash.iteritems())


