import pyperclip
import re
import matplotlib.pyplot as plt

s = pyperclip.paste

url_pattern = re.compile('Url (\d+)')
row_pattern = re.compile('	(\d+)	([\d\.]+)	\(\d+/\d+\)	([\d\.]+)?')

urls = {}
cur_url = None
for l in s.split("\n"):
    if l.startswith('Url'):
        m = url_pattern.match(l)
        cur_url = int(m.group(1))
        if not urls.get(cur_url):
            urls[cur_url] = []
    else:
        m = row_pattern.match(l)
        if m:
            hour = int(m.group(1))
            ctr = float(m.group(2))
            ratio = m.group(3)
            if ratio:
                urls[cur_url].append(float(ratio))

for url in urls:
    ratios = urls[url]
    plt.scatter(range(len(ratios)), ratios)
