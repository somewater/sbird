import numpy as np
from math import *
import pandas as pd

data = ["world science hello my", "I can hello", "sin cos arc x", "tg sin"]

def term_count(ws):
    wn = {}
    for w in ws:
        wn[w] = (wn.get(w) or 0) + 1
    return wn

def calc_idf(dd, uniq_ws):
    x = {}
    for w in uniq_ws:
        c = 0
        for d in dd:
            for w2 in d:
                if w2 == w:
                    c += 1
                    break
        x[w] = c
    return x

def parse(data):
    dd = [d.split(' ') for d in data]
    ws = [w for d in dd for w in d]
    uniq_ws = np.unique(ws)
    ws_ids = calc_idf(dd, uniq_ws)
    wn = term_count(ws)
    x = np.zeros((len(dd), len(uniq_ws)))
    for di, d in enumerate(dd):
        d_wn = term_count(d)
        for (w, c) in d_wn.items():
            wi = np.where(uniq_ws == w)[0]
            tf = float(c) / len(d)
            idf = log(float(len(data)) / ws_ids[w])
            x[di, wi] = tf * idf
    df = pd.DataFrame(data=x, index=[d[:10] + '..' for d in data], columns=uniq_ws)
    return df

X = parse(data)
U, s, V = np.linalg.svd(X.values)
S = np.zeros(X.values.shape)
S[:len(s), :len(s)] = np.diag(s)

U = pd.DataFrame(data=U, index=X.index)
V = pd.DataFrame(data=V, columns=X.columns)

X_ = np.dot(U.values, np.dot(S, V.values))
