import re
import sys
import os

WRITER_PATTERN = re.compile("Writer  \d+: (\d+)")
LOG_PATTERN = re.compile(".+_job\.\d+\.([\w\d\_]+)\.log(\.\d+)?")

def file_to_write_count(filepath):
    all_count = 0
    with open(filepath) as f:
        for line in f:
            # 12-10-2016 09:39:07 MSK tvrain INFO - 01:13 Writer  33979: 4235
            for write_count in WRITER_PATTERN.findall(line):
                all_count += int(write_count)
    return all_count

def process(df):
    dir = df
    calc = {}
    for filename in os.listdir(dir):
        filepath = "/".join([dir, filename])
        m = LOG_PATTERN.match(filepath)
        if m:
            job = m.group(1)
            writes = file_to_write_count(filepath)
            if not calc.get(job):
                calc[job] = 0
            calc[job] = writes + calc[job]
    for (job, writes) in calc.items():
        print "Job %s - %d" % (job, writes)

process(sys.argv[1])
