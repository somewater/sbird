import numpy as np
import scipy.stats
from statsmodels.stats.proportion import proportion_confint
import matplotlib.pyplot as plt
from math import *
import re
import pyperclip


def proportions_confint_diff_ind(sample1_p, sample1_len, sample2_p, sample2_len, alpha = 0.05):
    z = scipy.stats.norm.ppf(1 - alpha / 2.)

    if sample1_p < 1.0:
        p1 = sample1_p
    else:
        p1 = float(sample1_p) / sample1_len

    if sample2_p < 1.0:
        p2 = sample2_p
    else:
        p2 = float(sample2_p) / sample2_len

    left_boundary = (p1 - p2) - z * np.sqrt(p1 * (1 - p1)/ sample1_len + p2 * (1 - p2)/ sample2_len)
    right_boundary = (p1 - p2) + z * np.sqrt(p1 * (1 - p1)/ sample1_len + p2 * (1 - p2)/ sample2_len)

    return (left_boundary, right_boundary)

def proportions_confint_diff_rel(only_sample1_succ, only_sample2_succ, n, alpha = 0.05):
    z = scipy.stats.norm.ppf(1 - alpha / 2.)
    f = only_sample1_succ
    g = only_sample2_succ

    left_boundary = float(f - g) / n  - z * np.sqrt(float((f + g)) / n**2 - float((f - g)**2) / n**3)
    right_boundary = float(f - g) / n  + z * np.sqrt(float((f + g)) / n**2 - float((f - g)**2) / n**3)
    return (left_boundary, right_boundary)

def ab_test_results(s = None, alpha=0.05):
    if not s:
        s = pyperclip.paste()

    LINE = re.compile("\(([^,]+),([^,]+),([^,]+),([^,]+)\)")
    def parse_ab_result(line):
        """
        :return: (host, group, success_count, count)
        """
        if len(line) == 0:
            return None
        for host, group, count, ctr_persent in LINE.findall(line):
            return [host, int(group), int(int(count) * float(ctr_persent) * 0.01), int(count)]

    results = [r for r in [parse_ab_result(line) for line in s.split("\n") if len(line) > 0] if r]
    hosts = np.unique(map(lambda x: x[0], results))
    plt.clf()
    for host_idx, host in enumerate(hosts):
        plt.subplot(ceil(float(len(hosts)) / int(sqrt(len(hosts)))),
                    int(sqrt(len(hosts))),
                    host_idx + 1)

        host_results = sorted(filter(lambda x: x[0] == host, results),
                              key=lambda (h, g, s, c): float(s) / c,
                              reverse=True)
        experiments = filter(lambda x: x[1] != 0, host_results)
        _, _, control_success, control_count = filter(lambda x: x[1] == 0, host_results)[0]
        control_conf = proportion_confint(control_success, control_count, method='wilson', alpha=alpha)
        i = 0
        bar_params = []
        # bar_params.append([i, control_conf, 'control', 'yellow'])

        for experiment in experiments:
            i += 1
            _, exp_group, exp_success, exp_count = experiment
            # exp_conf = proportion_confint(exp_success, exp_count, method='wilson', alpha=alpha)
            exp_conf = proportions_confint_diff_ind(exp_success, exp_count, control_success, control_count)
            if np.all(np.array(exp_conf) > 0):
                color = 'green'
            elif np.all(np.array(exp_conf) < 0):
                color = 'red'
            else:
                color = 'yellow'
            # print "%s, group #%d - %s, %s" % (host, exp_group, str(exp_conf), color)

            bar_params.append([i, exp_conf, ("#%d" % exp_group), color])

        plt.bar(left=map(lambda x: x[0], bar_params),
                height=map(lambda x: x[1][1] - x[1][0], bar_params),
                width=0.5,
                bottom=map(lambda x: x[1][0], bar_params),
                tick_label=map(lambda x: x[2], bar_params),
                color=map(lambda x: x[3], bar_params))
        plt.xticks(rotation=90)
        plt.grid(True, axis='y')
        plt.axhline(y=0, color='k')
        plt.title(host)
    plt.show()
