# FROM CH Query:
#   SELECT
#     event_date,
#     referrer,
#     count(*) AS cnt
#   FROM
#     hits
#   WHERE
#     domain_id = 266
#     AND event_date >= toDate('2016-09-01')
#   GROUP BY event_date, referrer
#   ORDER BY event_date, COUNT () DESC
#   FORMAT TabSeparated

import os
import sys
import pandas
from datetime import date
import matplotlib.pyplot as plt

csv_path = sys.argv[1]
df = pandas.read_csv(csv_path, sep="\t", names=["date", "domain", "count"], header=None,
                     dtype={'date': date, 'domain': 'str', 'count': int},
                     parse_dates=['date'])
df = df.fillna('undefined')
df = df.pivot(index='date', columns='domain', values='count')
df = df.fillna(0)

for domain in df.columns:
    if df[domain].sum() < 1000:
        df.drop(domain, axis=1, inplace=True)

top_referrers = df.sum(axis=0).sort_values(ascending=False).keys()[0:10].tolist()
ax = df.plot()
handles, labels = ax.get_legend_handles_labels()
handles2 = []
labels2 = []
for i in range(len(labels)):
    lbl = labels[i]
    if lbl in top_referrers:
        labels2.append(lbl)
        handles2.append(handles[i])
plt.legend(handles2, labels2, loc='upper left')
plt.show()


import ipdb; ipdb.set_trace()