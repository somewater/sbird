import matplotlib.pyplot as plt
from matplotlib.dates import datestr2num
import numpy as np
import pandas as pd

cur = pd.read_csv("CURRFX-RUBUSD.csv")
oil = pd.read_csv("CHRIS-CME_BZ1.csv")

cur.columns = map(lambda x: x if x == 'Date' else "cur$" + x, cur.columns)
oil.columns = map(lambda x: x if x == 'Date' else "oil$" + x, oil.columns)

data = cur.merge(oil, on='Date', how='inner')
data['mDate'] = data['Date'].apply(datestr2num)

#plt.plot_date(data['mDate'], 3 * (1 / data['cur$Rate']) * data['oil$Open'], '-', label = 'ratio')
#plt.plot_date(data['mDate'], (1 / data['cur$Rate']) * 100, '-', label='RUB')
#plt.plot_date(data['mDate'], data['oil$Open'] * 100, '-', label='oil')

plt.plot_date(data['mDate'], (1 / data['cur$Rate']), '-', label='RUB')
plt.plot_date(data['mDate'], (3300 / data['oil$Open']), '-', label='RUB ex')

plt.grid()
plt.legend(loc='upper left')
plt.show()
