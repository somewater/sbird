#!/usr/bin/env python

import re
from dateutil import parser
from pylab import *

def lineToSeq(line):
    m = re.match(r'[^\s]+ ([^\s]+) .+DomainId  .+ end', line)
    if m:
        d = parser.parse(m.group(1))
        return d.hour * 60 + d.minute

def valsToHash(vals):
    h = {}
    for v in vals:
        h[v] = (h.get(v) or 0) + 1
    return h

def plot(vals):
    mn = min(vals)
    mx = max(vals)
    h = valsToHash(vals)
    plt.clf()
    diff = mx - mn
    diff_vals = map(lambda x: h.get(x) or 0, range(mn, mx))
    plt.plot(range(diff), diff_vals)
    plt.show()

with open(sys.argv[1]) as f:
    vals = [lineToSeq(l) for l in f]
    vals = [v for v in vals if v]
    plot(vals)